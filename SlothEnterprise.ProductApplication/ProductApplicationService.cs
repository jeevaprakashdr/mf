﻿using System;
using SlothEnterprise.ProductApplication.Applications;

namespace SlothEnterprise.ProductApplication
{
    public class ProductApplicationService : IProductApplicationService
    {
        private readonly IProductApplicationServiceFactory _factory;

        public ProductApplicationService(IProductApplicationServiceFactory factory)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        public int SubmitApplicationFor(ISellerApplication application)
        {
            if (application is null) throw new ArgumentNullException(nameof(application));

            return _factory
                .GetService(application.Product)
                .SubmitApplication(application);
        }
    }
}
