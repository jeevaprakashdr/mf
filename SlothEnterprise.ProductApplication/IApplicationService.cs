﻿using SlothEnterprise.ProductApplication.Applications;

namespace SlothEnterprise.ProductApplication
{
    public interface IApplicationService
    {
        int SubmitApplication(ISellerApplication application);
    }

    public interface IBusinessLoansApplicationService : IApplicationService
    {
    }

    public interface IConfidentialInvoiceApplicationService : IApplicationService
    {
    }
    public interface ISelectInvoiceApplicationService : IApplicationService
    {
    }
}