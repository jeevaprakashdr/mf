﻿using System;
using SlothEnterprise.ProductApplication.Products;

namespace SlothEnterprise.ProductApplication
{
    internal sealed class ProductApplicationServiceFactory : IProductApplicationServiceFactory
    {
        private readonly ISelectInvoiceApplicationService _selectInvoiceApplicationService;
        private readonly IConfidentialInvoiceApplicationService _confidentialInvoiceApplicationService;
        private readonly IBusinessLoansApplicationService _businessLoansApplicationService;
        public ProductApplicationServiceFactory(ISelectInvoiceApplicationService selectInvoiceApplicationService, 
            IConfidentialInvoiceApplicationService confidentialInvoiceApplicationService, 
            IBusinessLoansApplicationService businessLoansApplicationService)
        {
            _selectInvoiceApplicationService = selectInvoiceApplicationService ?? throw new ArgumentNullException(nameof(selectInvoiceApplicationService));
            _confidentialInvoiceApplicationService = confidentialInvoiceApplicationService ?? throw new ArgumentNullException(nameof(confidentialInvoiceApplicationService));
            _businessLoansApplicationService = businessLoansApplicationService ?? throw new ArgumentNullException(nameof(businessLoansApplicationService));
        }

        public IApplicationService GetService(IProduct product)
        {
            if (product is null) throw new ArgumentNullException(nameof(product));

            switch (product)
            {
                case SelectiveInvoiceDiscount _:
                    return _selectInvoiceApplicationService;
                case ConfidentialInvoiceDiscount _:
                    return _confidentialInvoiceApplicationService;
                case BusinessLoans _:
                    return _businessLoansApplicationService;
                default:
                    throw new InvalidSlothProductException();
            }
        }
    }
}