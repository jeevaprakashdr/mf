﻿using SlothEnterprise.ProductApplication.Products;

namespace SlothEnterprise.ProductApplication
{
    public interface IProductApplicationServiceFactory
    {
        IApplicationService GetService(IProduct product);
    }
}