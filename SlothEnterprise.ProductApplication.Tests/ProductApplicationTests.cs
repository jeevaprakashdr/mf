﻿using System;
using Autofac.Extras.Moq;
using AutoFixture;
using FluentAssertions;
using Moq;
using SlothEnterprise.ProductApplication.Applications;
using SlothEnterprise.ProductApplication.Products;
using Xunit;

namespace SlothEnterprise.ProductApplication.Tests
{
    public abstract class ProductApplicationTests
    {
        private readonly Fixture _fixture;
        private readonly AutoMock _container;
        
        protected ProductApplicationTests()
        {
            _fixture = new Fixture();
            _container = AutoMock.GetStrict();
            _container.VerifyAll = true;
        }

        // Class names describes WHEN section of the test method
        public sealed class ConstructorTests : ProductApplicationTests
        {
            [Fact]
            public void GivenProductApplicationServiceFactoryIsNull_ThenArgumentNullExceptionIsThrown()
            {
                FluentActions
                    .Invoking(() => new ProductApplicationService(factory: null))
                    .Should().ThrowExactly<ArgumentNullException>()
                    .Which.ParamName.Should().Be("factory");
            }

            [Fact]
            public void GivenValidArguments_ThenNoExceptionIsThrown()
            {
                FluentActions
                    .Invoking(() =>
                        new ProductApplicationService(
                            _container.Mock<IProductApplicationServiceFactory>().Object))
                    .Should().NotThrow();
            }
        }

        // Class names describes WHEN section of the test method
        public sealed class SubmitApplicationForTests : ProductApplicationTests
        {
            private readonly Mock<IApplicationService> _applicationServiceMock;

            public SubmitApplicationForTests()
            {
                _applicationServiceMock = _container.Mock<IApplicationService>();
            }

            [Fact]
            public void GivenApplicationIsNull_ThenArgumentNullExceptionIsThrown()
            {
                FluentActions
                    .Invoking(() => _container
                        .Create<ProductApplicationService>()
                        .SubmitApplicationFor(application: null))
                    .Should().ThrowExactly<ArgumentNullException>()
                    .Which.ParamName.Should().Be("application");
            }

            [Fact]
            public void GivenConfidentialInvoiceDiscount_ShouldReturnOne()
            {
                // Given
                var resultValue = _fixture.Create<int>();
                var sellerApplicationMock = _container.Mock<ISellerApplication>();
                
                sellerApplicationMock
                    .SetupProperty(application => application.Product, _fixture.Create<ConfidentialInvoiceDiscount>())
                    .SetupProperty(application => application.CompanyData, _fixture.Create<SellerCompanyData>());

                SetupProductApplicationServiceFactory(sellerApplicationMock);
                SetupApplicationService(sellerApplicationMock, resultValue);

                // When Then
                _container
                    .Create<ProductApplicationService>()
                    .SubmitApplicationFor(_container.Mock<ISellerApplication>().Object)
                    .Should().Be(resultValue);
            }

            [Fact]
            public void GivenSelectiveInvoiceDiscount_ShouldReturnOne()
            {
                // Given
                var resultValue = _fixture.Create<int>();
                var sellerApplicationMock = _container.Mock<ISellerApplication>();
                
                sellerApplicationMock
                    .SetupProperty(application => application.Product, _fixture.Create<SelectiveInvoiceDiscount>())
                    .SetupProperty(application => application.CompanyData, _fixture.Create<SellerCompanyData>());

                SetupProductApplicationServiceFactory(sellerApplicationMock);
                SetupApplicationService(sellerApplicationMock, resultValue);

                // When Then
                _container
                    .Create<ProductApplicationService>()
                    .SubmitApplicationFor(_container.Mock<ISellerApplication>().Object)
                    .Should().Be(resultValue);
            }

            [Fact]
            public void GivenBusinessLoans_ShouldReturnOne()
            {
                // Given
                var resultValue = _fixture.Create<int>();
                var sellerApplicationMock = _container.Mock<ISellerApplication>();
                
                sellerApplicationMock
                    .SetupProperty(application => application.Product, _fixture.Create<BusinessLoans>())
                    .SetupProperty(application => application.CompanyData, _fixture.Create<SellerCompanyData>());
                
                SetupProductApplicationServiceFactory(sellerApplicationMock);
                SetupApplicationService(sellerApplicationMock, resultValue);
                
                // When Then
                _container
                    .Create<ProductApplicationService>()
                    .SubmitApplicationFor(_container.Mock<ISellerApplication>().Object)
                    .Should().Be(resultValue);
            }

            private void SetupProductApplicationServiceFactory(Mock<ISellerApplication> sellerApplicationMock) =>
                _container
                    .Mock<IProductApplicationServiceFactory>()
                    .Setup(factory => factory.GetService(sellerApplicationMock.Object.Product))
                    .Returns(_applicationServiceMock.Object);

            private void SetupApplicationService(Mock<ISellerApplication> sellerApplicationMock, int resultValue) =>
                _applicationServiceMock
                    .Setup(service => service.SubmitApplication(sellerApplicationMock.Object))
                    .Returns(resultValue);
        }

        internal sealed class InValidProduct : IProduct
        {
            public int Id { get; }
        }
    }
}