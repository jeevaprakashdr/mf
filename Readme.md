I have done my best to come up with the solution maintain simplicity.
Would like to mention some of my approaches.
1. Unit Test following Given When Then pattern. 
2. Unit tests are grouped based on the feature of a class under test. This will describe 'When' section of the test.
3. Autofixture has been used for generating the test data.
4. Autofac.Moq framework is used create mock objects. I have followed Strict behaviour and by default, all mock fixtures need to have a setup.