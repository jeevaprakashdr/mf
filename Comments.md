1. No guard clause for ProductApplicationService constructor and method SubmitApplicationFor  arguments.
2. SubmitApplicationFor throw InvalidOperationException, which could be something like Invalid Product or Product Service Can not be found.
3. As the problem states ProductApplicationService is not extensible, because 
* Insead of acting as a request router, it is doing more than one thing at a time, violating SRP.
	- Identify the product type.
	- Submit application request.
	- Validate the response response (if result is success or applicationId is not null)
4. Method SubmitApplicationFor returns a non positive value in case of request failure. It could return ApplicationResult for better handling of failure at high in invocation lader.
5. In ProductApplicationTest 
* It does not cover all the product micorservice.
* Sut is on mock objects.
* Current test is not valid or clear, because it sounds like to be for SelectInvoiceService accepting SelectiveInvoiceDiscount as product, instead testing ConfidentialInvoiceService accepting mock of ConfidentialInvoiceDiscount.

